import Keycloak from "../";
import {SET_AUTHENTICATED} from "./types.js";

const initialState = {
    keycloak: kc,
    authenticated: false
}

export default function keycloakReducer(state = initialState, action) {
    switch(action.type) {
        case SET_AUTHENTICATED:
            return {
                ...state,
                keycloak: action.payload.keycloak
            };
        default:
            return state;
    }
}
